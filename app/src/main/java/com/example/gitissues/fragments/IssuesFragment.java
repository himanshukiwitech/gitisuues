package com.example.gitissues.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.gitissues.R;
import com.example.gitissues.adapter.IssuesListAdapter;
import com.example.gitissues.interfaces.OnLoadMoreListener;
import com.example.gitissues.pojo.Issue;
import com.example.gitissues.retrofit.IssuesService;
import com.example.gitissues.retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssuesFragment extends Fragment {

    private RecyclerView recyclerView;

    private List<Issue> issues = new ArrayList<>();

    private IssuesListAdapter issuesListAdapter;

    private int currentPage = 1;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_issues, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView = getView().findViewById(R.id.recycler_view);
        progressBar = getView().findViewById(R.id.progress_loader);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        issuesListAdapter = new IssuesListAdapter(getActivity(), recyclerView, issues);
        recyclerView.setAdapter(issuesListAdapter);

        loadIssues();

        issuesListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadIssues();
            }
        });
    }

    private void loadIssues() {
        showProgress(true);
        boolean isOpen = false;
        if(getArguments() != null) {
            isOpen = getArguments().getBoolean("isOpen", false);
        }
        IssuesService issuesService = RetrofitClientInstance.getRetrofitInstance().create(IssuesService.class);
        Call<List<Issue>> issuesCall = null;
        if(isOpen) {
            issuesCall = issuesService.getOpenIssues(currentPage);
        } else {
            issuesCall = issuesService.getClosedIssues(currentPage);
        }
        issuesCall.enqueue(new Callback<List<Issue>>() {
            @Override
            public void onResponse(Call<List<Issue>> call, Response<List<Issue>> response) {
                issues.addAll(response.body());
                issuesListAdapter.notifyDataSetChanged();
                //stop load pages if reach to end
                if(response.body() != null && response.body().size() != 0) {
                    issuesListAdapter.setLoaded();
                }
                currentPage++;
                showProgress(false);
            }

            @Override
            public void onFailure(Call<List<Issue>> call, Throwable t) {
                t.printStackTrace();
                issuesListAdapter.setLoaded();
                showProgress(false);
            }
        });
    }
    
    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
}