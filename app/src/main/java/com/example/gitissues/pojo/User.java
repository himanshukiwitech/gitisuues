package com.example.gitissues.pojo;

import com.google.gson.annotations.SerializedName;

public  class User {
    @SerializedName("login")
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
