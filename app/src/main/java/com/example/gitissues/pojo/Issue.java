package com.example.gitissues.pojo;

import com.google.gson.annotations.SerializedName;

public class Issue {
    @SerializedName("id")
    private Long id;

    @SerializedName("number")
    private Long number;

    @SerializedName("title")
    private String title;

    @SerializedName("body_html")
    private String bodyHtml;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("state")
    private String state;

    @SerializedName("user")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyHtml() {
        return bodyHtml;
    }

    public void setBodyHtml(String bodyHtml) {
        this.bodyHtml = bodyHtml;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
