package com.example.gitissues;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.gitissues.adapter.TabAdapter;
import com.example.gitissues.fragments.IssuesFragment;
import com.example.gitissues.pojo.Issue;
import com.example.gitissues.retrofit.IssuesService;
import com.example.gitissues.retrofit.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager());

        IssuesFragment openIssueFragment = new IssuesFragment();
        Bundle args = new Bundle();
        args.putBoolean("isOpen", true);
        openIssueFragment.setArguments(args);
        adapter.addFragment(openIssueFragment, "Open");

        adapter.addFragment(new IssuesFragment(), "Closed");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
