package com.example.gitissues.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gitissues.IssueDetailActivity;
import com.example.gitissues.R;
import com.example.gitissues.interfaces.OnLoadMoreListener;
import com.example.gitissues.pojo.Issue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class IssuesListAdapter extends RecyclerView.Adapter<IssuesListAdapter.ViewHolder>{
    private final RecyclerView recyclerView;
    private final Activity activity;
    private List<Issue> issues;

    private SimpleDateFormat simpleDateFormat = new
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private SimpleDateFormat visibleDateFormat =
            new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    private OnLoadMoreListener onLoadMoreListener;

    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public IssuesListAdapter(Activity activity, RecyclerView recyclerView, List<Issue> issues) {
        this.issues = issues;
        this.recyclerView = recyclerView;
        this.activity = activity;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.item_issue_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Issue issue = issues.get(position);
        holder.issueTitle.setText(issue.getTitle());
        holder.issueNumber.setText("#" + issue.getNumber());
        if(issue.getUser() != null) {
            holder.userName.setText("Created by " + issue.getUser().getLogin());
        } else {
            holder.userName.setText("");
        }
        try {
            Date date = simpleDateFormat.parse(issue.getCreatedAt());
            holder.createdDate.setText("Created on " + visibleDateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            holder.createdDate.setText("");
        }

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(recyclerView.getContext(), IssueDetailActivity.class);
                intent.putExtra("html", issue.getBodyHtml());
                activity.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return issues.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView issueTitle;
        public TextView issueNumber;
        public TextView userName;
        public TextView createdDate;
        public ViewGroup parent;
        public ViewHolder(View itemView) {
            super(itemView);
            this.issueTitle = itemView.findViewById(R.id.issue_title);
            this.issueNumber = itemView.findViewById(R.id.issue_number);
            this.userName = itemView.findViewById(R.id.user_name);
            this.createdDate = itemView.findViewById(R.id.created_date);
            parent = itemView.findViewById(R.id.item_parent);
        }
    }
}  