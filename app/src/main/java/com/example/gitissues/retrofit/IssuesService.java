package com.example.gitissues.retrofit;

import com.example.gitissues.pojo.Issue;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface IssuesService {

    @Headers({"Accept:application/vnd.github.VERSION.full+json",
        "Authorization:Basic aGltYW5zaHVtb2h0YS1raXdpdGVjaDpraXdpNDMyMQ=="})
    @GET("/repos/alamofire/alamofire/issues?state=open")
    Call<List<Issue>> getOpenIssues(@Query("page") int page);

    @Headers({"Accept:application/vnd.github.VERSION.full+json",
            "Authorization:Basic aGltYW5zaHVtb2h0YS1raXdpdGVjaDpraXdpNDMyMQ=="})
    @GET("/repos/alamofire/alamofire/issues?state=closed")
    Call<List<Issue>> getClosedIssues(@Query("page") int page);
}