package com.example.gitissues;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

public class IssueDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_detail);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String html = intent.getStringExtra("html");
        html = "<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" /></head><body>"
                + html + "</body></html>";
        WebView webView = findViewById(R.id.web_view);
        webView.loadDataWithBaseURL("file:///android_asset/",
                html, "text/html", "UTF-8", null);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
