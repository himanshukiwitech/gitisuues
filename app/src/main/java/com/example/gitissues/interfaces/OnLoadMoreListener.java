package com.example.gitissues.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore();
}